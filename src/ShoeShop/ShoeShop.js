import React, { Component } from "react";
import Cart from "./Cart";
import Modal from "./Modal";
import ProductList from "./ProductList";

export default class ShoeShop extends Component {
  render() {
    return (
      <div>
        <h2>Shoes Shop</h2>
        <div className="row m-0">
          <div className="col-6">
            <ProductList />
          </div>
          <div className="col-6 cart">
            <Cart />
          </div>
        </div>
        <Modal />
      </div>
    );
  }
}
