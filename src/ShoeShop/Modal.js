import React, { Component } from "react";
import { connect } from "react-redux";

class Modal extends Component {
  render() {
    let { image, name, price, description } = this.props.productDetail;
    return (
      <div>
        {/* Modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  {name}
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="pl-5 pr-5 shadow">
                  <img style={{ width: "100%" }} src={image} alt="" />
                </div>
                <div className="d-flex flex-column align-items-start mt-3">
                  <h5>{price}$</h5>
                  <p className="text-left">{description}</p>
                </div>
                <div></div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    productDetail: state.shoeReducer.productDetail,
  };
};

export default connect(mapStateToProps)(Modal);
