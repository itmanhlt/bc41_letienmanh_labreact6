import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ProductList extends Component {
  render() {
    return (
      <div className="row mt-0">
        {this.props.productList.map((item, index) => {
          return <ItemShoe key={index} item={item} />;
        })}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    productList: state.shoeReducer.productList,
  };
};

export default connect(mapStateToProps)(ProductList);
