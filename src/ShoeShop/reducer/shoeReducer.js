import { dataShoe } from "../dataShoe";

let initialValue = {
  productList: dataShoe,
  cart: [],
  productDetail: [],
};

export const shoeReducer = (state = initialValue, action) => {
  let handleDelete = () => {
    if (window.confirm("Are you want to delete your product?") === true) {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload;
      });
      cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
  };

  let handleChangeQuantity = () => {
    let cloneCart = [...state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === action.payload.id;
    });
    if (action.payload.quantity === 1) {
      cloneCart[index].quantity += action.payload.quantity;
    } else {
      cloneCart[index].quantity > 1
        ? (cloneCart[index].quantity += action.payload.quantity)
        : handleDelete() && cloneCart.splice(index, 1);
    }
    return { ...state, cart: cloneCart };
  };
  switch (action.type) {
    case "ADD_TO_CART": {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      if (index === -1) {
        let newShoe = { ...action.payload, quantity: 1 };
        cloneCart.push(newShoe);
      } else {
        cloneCart[index].quantity++;
      }
      return { ...state, cart: cloneCart };
    }

    case "DELETE_TO_CART": {
      return handleDelete();
    }

    case "CHANGE_QUANTITY": {
      return handleChangeQuantity();
    }

    case "SHOW_DETAIL": {
      return { ...state, productDetail: action.payload };
    }
    default:
      return state;
  }
};
